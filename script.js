document.addEventListener('DOMContentLoaded', function () {
    const countryList = document.getElementById('country-list');

    // Fetching data from API
    fetch('https://restcountries.com/v3.1/all')
        .then(response => response.json())
        .then(data => {
            // Loop through each country in the response
            data.forEach(country => {
                // Create a card for each country
                const countryCard = document.createElement('article');
                countryCard.classList.add('country-card');

                // Country name
                const countryName = document.createElement('h2');
                countryName.textContent = country.name.common;
                countryCard.appendChild(countryName);

                // Flag image
                if (country.flags && country.flags.png) {
                    const flagImg = document.createElement('img');
                    flagImg.src = country.flags.png;
                    flagImg.alt = `${country.name.common} flag`;
                    countryCard.appendChild(flagImg);
                }

                // Append the country card to the country list
                countryList.appendChild(countryCard);
            });
        })
        .catch(error => {
            console.error('Error fetching data:', error);
        });
});
